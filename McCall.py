# -*- coding: utf-8 -*-
"""
Created on Thu Feb 18 16:13:52 2016

@author: David Evans
"""

import numpy as np

def bellmanTmap(beta,w,pi,b,alpha,chi,Vcont,Qcont):
    '''
    Iterates of Bellman Equation in Problem 1
    See Homework 3.pdf for Documentation
    '''
    V = np.zeros(len(pi))
    C = np.zeros(len(pi))

    noeffort = b + beta*((.3*pi.dot(Vcont))+(.7*Qcont))
    effort = (b-chi) + beta*((.5*pi.dot(Vcont))+(.5*Qcont))
    Q = np.maximum(noeffort, effort)

    if Q == effort:
        E = True
    else:
        E = False

    employed = w + beta*(alpha*Qcont+(1-alpha)*Vcont)
    
    for s in range(len(pi)):
        V[s] = np.maximum(employed[s], Q)
        if V[s] == employed[s]:
            C[s] = 1
        else:
            C[s] = 0

    return V, Q, C, E 
    
    
def solveInfiniteHorizonProblem(beta,w,pi,b,alpha,chi,epsilon=1e-8):
    '''
    Solve infinite horizon workers problem in Problem 1
    See Homework 3.pdf for Documentation
    '''
    V = np.zeros(len(pi))
    Q = 0

    T_V, T_Q, C, E = bellmanTmap(beta,w,pi,b,alpha,chi,V,Q)
    
    while np.linalg.norm(V-T_V) > epsilon:
        V, Q = T_V, T_Q
        T_V, T_Q, C, E = bellmanTmap(beta,w,pi,b,alpha,chi,V,Q)
        
    return V, Q, C, E
    
def HazardRate(pi,C,E):
    '''
    Computes the hazard rate of leaving unemployment in Problem 1
    See Homework 3.pdf for Documentation
    '''
    if E == 1:
        p_offer = .5
    else:
        p_offer = .3
        
    hrate = pi.dot(C)*(p_offer)
    return hrate
        
        
def bellmanTmap_HC(beta,w,pi,b,alpha,p_h,h,Vcont,Qcont):
    '''
    Iterates of Bellman Equation of Problem 2
    See Homework 3.pdf for Documentation
    '''
    J = len(Qcont)
    S = len(pi)
    Q = np.zeros(J)
    V = np.zeros((J, S))
    v_emp = np.zeros((J, S))
    C = np.zeros((J, S)) 
    
    
    Q[0] = b + beta*(pi.dot(Vcont[0,:]))
    
    for j in range(J):
        if j < J-1 :
            Q[j+1] = b + beta*(p_h*pi.dot(Vcont[j, :]) + (1-p_h)*pi.dot(Vcont[j+1, :]))
            #x is suppose to be the human capital level when employed
            #y is suppose to be the human capital when unemployed
            x = j+1
            y = j
        else:
            x = j
            y = j
            
        fire = p_h*Qcont[x] + (1-p_h)*Qcont[y]
        emp =(p_h*Vcont[x, :] + (1-p_h)*Vcont[y, :])
        v_emp[j, :] = w*h[y] + beta*((alpha*fire)+ ((1-alpha)*emp))
        V[j, :] = np.maximum(v_emp[j, :], Q[j])

        for s in range(S):
            if V[j, s] == v_emp[j, s]:
                C[j, s] = 1


    return V, Q, C
    

def solveInfiniteHorizonProblem_HC(beta,w,pi,b,alpha,hprob,hgrid,epsilon=1e-8):
    '''
    Solve infinite horizon workers problem of Problem 2
    See Homework 3.pdf for Documentation
    '''

    J = len(hgrid)
    S = len(pi)
    
    Q = np.zeros(J)
    V = np.zeros((J, S))
    C = np.zeros((J, S))

    T_V, T_Q, T_C = bellmanTmap_HC(beta,w,pi,b,alpha,hprob,hgrid,V,Q)
    
    while np.linalg.norm(V-T_V) > epsilon:
        V,Q,C = T_V, T_Q, T_C
        T_V, T_Q, T_C = bellmanTmap_HC(beta,w,pi,b,alpha,hprob,hgrid,V,Q)
        
    return V, Q, C

    
    
    
    
    
    
    
    